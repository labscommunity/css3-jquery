TALLER CODELABS CSS3 - JQUERY

@LabsCommunity

@Bootmack @iTheCode @Drakho
@paulrrdiaz

-----------------------------------


>		//.css -> alterar las propiedades del css
>		//.addClass -> agregar una clase
>		//.removeClass -> remover una clase
>		// EVENTOS
>		//.click -> evento click
>		//.fadeIn -> efecto fade de entrada
>		//.fadeOut -> efecto fade de salida
>		//.fadeIn(time)
>		//.fadeOut(time)
>		//.fadeToggle(time) -> une fadeIn y fadeOut
>		//.slideDown(time) -> deslizamiento para abajo
>		//.slideUp(time) -> deslizamiento para arriba
>		//.slideToggle(time) -> une el slideDown y slideUp
>		// SELECTORES
>		//.children('li') -> selecciona a los elementos li hijos del elemento
>		//.find('li') -> selecciona a todos los elementos li
>		//.next() -> selecciona el elemento siguiente
>		//.hide() -> oculta al elemento seleccionado
>		//.show() -> muestra al elemento seleccionado